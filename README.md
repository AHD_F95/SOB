# Community translation of SOB

Welcome to the community translation of SOB. We want to make sure this wonderful game is available to people that do not speak Russian. 

It is important to know that the major portion of the text was first translated by a computer algorithm, creating something that should be English. 
This algorithm is not really good at 'our type of text' and the result is not really nice to read through. So, we need your help in fixing that!

## Nearly everything is machine translated first. Then the main job of translators/editors is to sort out this machine lingo and turn it into something nice and readable.

Jan 2019: Bits and pieces are finished, but a large part of the text will still need to go through a human interpretor. This will take awhile to be fully complete.

If you would like to help, fork this repo into your own repository and look in the cheats directory for a file to work on. Make sure to read the contributing guide well!

## Character name changes.
* Glad/Happy --- Rada
* Eugene ------- Eugenia
* Light -------- Sveta
* Kate --------- Katya
* Aurelius ----- Aurelia
* Baena -------- Bazhena
* Jan/Ian ------ Yana
* Twins -------- Daria & Maria

## Some important changes
Katya's father's questions have been fixed into a much simpler version. Choices are now A, B, C or D and questions are clear.

## Scripts
These are for devs to split the main qsp into separate files, wich is a lot easier to work on with a more advanced editor such as Notepad++.
Then another script is used to merge the split files back into one large file needed by the game itself.

If you would like to test your own translation, then run one of the scripts or the .bat (Window$)

## vicezealot mod files
Unzip into the unique_npc folder of the game directory.

## Bonus scenes 1 and 2:
Download from https://mega.nz/#!mq5hXYgS!BOgSFACaDkqz1oBR_pLh3BjBFa8VAyAdMERuKMMc8nM
